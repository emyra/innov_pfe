/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class ListeSoutenanceController implements Initializable {

    @FXML
    private AnchorPane anchorredirect;
    @FXML
    private TableView<?> tableevenement;
    @FXML
    private TableColumn<?, ?> idevent;
    @FXML
    private TableColumn<?, ?> nomevenement;
    @FXML
    private TableColumn<?, ?> desc;
    @FXML
    private TableColumn<?, ?> contact;
    @FXML
    private TableColumn<?, ?> nbbbbb;
    @FXML
    private TableColumn<?, ?> prix;
    @FXML
    private TableColumn<?, ?> lieux;
    @FXML
    private TableColumn<?, ?> debut;
    @FXML
    private TableColumn<?, ?> fin;
    @FXML
    private ImageView image;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
