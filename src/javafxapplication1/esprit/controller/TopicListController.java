/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import IServices.esprit.ITopicService;
import com.esprit.entity.Quizz;
import com.esprit.entity.Topic;
import com.esprit.entity.Utilisateur;
import com.esprit.services.QuizzServiceImpl;
import com.esprit.services.TopicServiceImpl;
import com.esprit.services.UtilisateurService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;


/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class TopicListController implements Initializable {
    
    public Tab topicListTab;
    
    

    @FXML
    private AnchorPane anchorredirect;
    @FXML
    private TableView<Topic> topicTable;
    @FXML
    private TableColumn<Topic, String> title;
    @FXML
    private TableColumn<?, ?> description;
    @FXML
    private TableColumn<?, ?> tag;
    @FXML
    private TableColumn<?, ?> creator;
   
    @FXML
    private ImageView image;
    
    public ObservableList<Topic> data = FXCollections.observableArrayList(
    
            
);
    
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        List<Topic> topicList =  TopicServiceImpl.getInstance().getAll();
        List<Quizz> quizzList = QuizzServiceImpl.getInstance().getAll();
        Quizz quizz = QuizzServiceImpl.getInstance().getQuizzById(1);
        topicList.forEach(topic ->{
            int creatorId = topic.getCreatorId();
            Utilisateur creator = UtilisateurService.getInstance().getUserById(creatorId);
            topic.setCreator(creator);
            
        });
        data = FXCollections.observableArrayList(topicList);
        
        title.setCellValueFactory(
            new PropertyValueFactory("title")
        );
        description.setCellValueFactory(
            new PropertyValueFactory("description")
        );
        tag.setCellValueFactory(
            new PropertyValueFactory("tag")
        );
        creator.setCellValueFactory(
            new PropertyValueFactory("creatorName")
        );
        
        
        topicTable.setItems(data);
        topicTable.setRowFactory( tv -> {
            TableRow<Topic> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                Topic rowData = row.getItem();
                System.out.println(rowData);
               
                redirectToTopicView(rowData);
                

        
            }
            });
            return row ;
        });
    }    
    
    @FXML
    private void add(ActionEvent event) {
    }
    
    
    private void redirectToTopicView(Topic currentTopic){
        
        try {
            URL url  = getClass().getClassLoader().getResource( "javafxapplication1/TopicView.fxml" );
            
            
            FXMLLoader loader = new FXMLLoader(url);

        // Create a controller instance
        TopicController controller = new TopicController();
        controller.currentTopic = currentTopic;
        // Set it in the FXMLLoader
        loader.setController(controller);
            
            Parent root = loader.load();
            
            topicListTab.setContent(root);
                    
        } catch (IOException ex) {
            Logger.getLogger(TopicListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
