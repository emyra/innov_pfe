/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.MediaView;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class acceuilController implements Initializable {

    @FXML
    private AnchorPane eve;
    @FXML
    private ImageView image;
    @FXML
    private MenuButton soutenance;
    @FXML
    private Label lblprenom;
    @FXML
    private Label lblnom;
    @FXML
    private Button guide;
    @FXML
    private Button propos;
    @FXML
    private Button contact;
    @FXML
    private Button description;
    @FXML
    private Button acceuil;
    @FXML
    private MediaView mv;
    @FXML
    private ImageView info;
    @FXML
    private ImageView image1;
    @FXML
    private ImageView image2;
    @FXML
    private ImageView image11;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img11;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void mesevenements(ActionEvent event) {
    }

    @FXML
    private void pause(MouseEvent event) {
    }

    @FXML
    private void play(ScrollEvent event) {
    }
    
}
