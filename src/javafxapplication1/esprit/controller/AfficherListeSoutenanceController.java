/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class AfficherListeSoutenanceController implements Initializable {

    @FXML
    private TableView<?> table;
    @FXML
    private TableColumn<?, ?> nbrreserv;
    @FXML
    private TableColumn<?, ?> confirmation;
    @FXML
    private TableColumn<?, ?> id;
    @FXML
    private TableColumn<?, ?> date;
    @FXML
    private Label labelnbrreserv1;
    @FXML
    private Label labelconfirmation1;
    @FXML
    private Label nomeve;
    @FXML
    private Label nomuser;
    @FXML
    private Button actualiser;
    @FXML
    private Label labelnbrreserv;
    @FXML
    private Label labelconfirmation;
    @FXML
    private Label labelid;
    @FXML
    private Label labeliduser;
    @FXML
    private Label labelidevenement;
    @FXML
    private Label labeldate;
    @FXML
    private Button imprimer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void modifier(ActionEvent event) {
    }

    @FXML
    private void loadTableViewReserservations(ActionEvent event) {
    }

    @FXML
    private void supprimer(ActionEvent event) {
    }

    @FXML
    private void imprimerReservation(ActionEvent event) {
    }

    @FXML
    private void ajouter(ActionEvent event) {
    }
    
}
