/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author sahar
 */

@Table
@Entity
public class CandidateQuizz {
    
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name= "id")
   private int id;
   
   @Column(name= "candidateId")
    private int candidateId;
   @Column(name= "quizzId")
    private int quizzId;
   
   @Column(name= "topicId")
    private int topicId;
   
   @Column(name = "score")
   private int score;
    
   
   @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name= "fk_candidateQuizz_id")
    private List<CandidateResponse> responses;
   
   
   public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    public int getTopicId(){
        return topicId;
    }
    
    public void setTopicId(int topicId){
        this.topicId = topicId;
    }
    
    public int getQuizzId(){
        return quizzId;
    }
    
    public void setQuizzId(int quizzId){
        this.quizzId = quizzId;
    }
    
    
    public int getCandidateId(){
        return candidateId;
    }
    
    public void setCandidateId(int candidateId){
        this.candidateId = candidateId;
    }
    
    public int getScore(){
        return score;
    }
    
    public void setScore(int score){
        this.score = score;
    }
    
    public List<CandidateResponse> getResponses(){
        return responses;
    }
    
    public void setResponses(List<CandidateResponse> responses){
        this.responses = responses;
    }
    

}
