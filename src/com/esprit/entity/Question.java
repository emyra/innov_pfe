/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author sahar
 */
@Table
@Entity
public class Question {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private int id;
    
    @Column(name= "name")
    private String name;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name= "fk_question_id")
    private List<Choice> choices;
    
    @Column(name="fk_quizz_id")
    private int quizzId;
    
    @Column(name= "result")
    private int result;
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    
    public int getQuizzId(){
        return quizzId;
    }
    
    public void setQuizzId(int quizzId){
        this.quizzId = quizzId;
    }
    
    
    public List<Choice> getChoices(){
        return choices;
    }
    
    public void setChoices(List<Choice> choices){
        this.choices = choices;
    }
    
    
    public int getResult(){
        return result;
    }
    
    public void setResult(int result){
        this.result = result;
    }
    
}
