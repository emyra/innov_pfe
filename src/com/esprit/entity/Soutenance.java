/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author sahar
 */
public class Soutenance {
    private int id;
    private int candidateId;
    private int supervisor;
    private int topic;
    private List<Utilisateur> jury;
    private String date;
    // Enum : VALIDE NON_VALIDE
    private String result;
    private String salle; 

    public Soutenance() {
    }

    public Soutenance(int id, int candidateId, int supervisor, int topic, String date, String result, String salle) {
        this.id = id;
        this.candidateId = candidateId;
        this.supervisor = supervisor;
        this.topic = topic;
       
        this.date = date;
        this.result = result;
        this.salle = salle;
    }

   
   
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public int getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(int supervisor) {
        this.supervisor = supervisor;
    }

    public int getTopic() {
        return topic;
    }

    public void setTopic(int topic) {
        this.topic = topic;
    }

    public List<Utilisateur> getJury() {
        return jury;
    }

    public void setJury(List<Utilisateur> jury) {
        this.jury = jury;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSalle() {
        return salle;
    }

    public void setSalle(String salle) {
        this.salle = salle;
    }
    
     @Override
    public String toString() {
        return "Soutenance{" + "id=" + id + ", candidateId=" + candidateId + ", supervisor=" + supervisor + ", topic=" + topic + ", jury=" + jury + ", date=" + date + ", result=" + result + ", salle=" + salle + '}';
    }

}
