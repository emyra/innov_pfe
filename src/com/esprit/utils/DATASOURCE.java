/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.utils;

import com.esprit.entity.Personne;
import connexionbd.ConnexionBD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Djoo
 */
public class DATASOURCE {
    
   private String login="root";
    private String pwd="";
    private String url="jdbc:mysql://127.0.0.1:3306/esprit"; 
    private Connection cnx;

    private static DATASOURCE instance;

    public DATASOURCE() {
         try {
            // TODO code application logic here
            cnx=DriverManager.getConnection(url, login, pwd);
           
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnexionBD.class.getName()).log(Level.SEVERE, null, ex);
          //  System.out.println(ex.getMessage());
        }
    }

    public static DATASOURCE getInstance() {
        if(instance==null)
            instance=new DATASOURCE();
        return instance;
    }

    public Connection getCnx() {
        return cnx;
    }
    
    
}
