/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.services;
 import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
/**
 *
 * @author asus
 */
public class MailServiceImpl {
    
   public static MailServiceImpl instance;

    public static MailServiceImpl getInstance(){
        if(instance == null)
            instance = new MailServiceImpl();
        return instance;
    }


   public boolean sendQuizzResult(int score, String mail) {    
      // Recipient's email ID needs to be mentioned.
      String to = mail;

      // Sender's email ID needs to be mentioned
      String from = "emyra.guerfali.esprit@gmail.com";

      // Assuming you are sending email from localhost
      String host = "smtp.gmail.com";

      // Get system properties
      Properties properties = System.getProperties();

      // Setup mail server
      properties.setProperty("mail.smtp.host", host);
      
      properties.put("mail.smtp.starttls.enable", "true");
      properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
     
        properties.setProperty("mail.smtp.auth", "true"); 

        // Get the default Session object.
         Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() 
    {
        protected PasswordAuthentication getPasswordAuthentication() 
        {
            return new PasswordAuthentication("emyra.guerfali.esprit","Esprit2018!?");
        }
   });
      
     boolean sent = false;
     if(mail != null){
      try {
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

         // Set Subject: header field
         message.setSubject("Esprit Quizz Result");

         if(score>=50)
         // Now set the actual message
            message.setText("Congratulations, you passed the quiz with a score "+ score +"%");
         else
             message.setText("Unfortunately , you didn't succeed the quizz, your score is "+ score + "%");

         // Send message
         Transport.send(message);
         sent = true;
         System.out.println("Sent message successfully....");
      } catch (MessagingException mex) {
          sent = false;
         mex.printStackTrace();
      }
     }
      return sent;
   }

    
}
