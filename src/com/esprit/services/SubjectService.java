/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.services;


import IServices.esprit.ISubjectService;
import IServices.esprit.IUtilisateurService;
import com.esprit.entity.Subject;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Kaisser
 */
public abstract class SubjectService implements ISubjectService{
 private Connection conn;
    @Override
    public void add(Subject r) {
        
        try {
            java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
            String query = "insert into subject( id, name) values( ?, ?)";
            PreparedStatement ps = conn.prepareStatement(query);
           
            ps.setInt(1, r.getId());
           
            ps.setString(2, r.getName());
            
            ps.executeUpdate();
            System.out.println("l'Ajout effectuer avec succées");
        } catch (SQLException ex) {
            Logger.getLogger(SubjectService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public SubjectService (){
        conn = DATASOURCE.getInstance().getCnx();
        
    }

    

    @Override
    public void delete(int id) {
        try {
         String query = "DELETE FROM subject where id =?";
         PreparedStatement ps = conn.prepareStatement(query);
         ps.setInt(1,id);
         ps.executeUpdate();
            System.out.println("Suppression effectué");
     } catch (SQLException ex) {
         Logger.getLogger(SubjectService.class.getName()).log(Level.SEVERE, null, ex);
     }
        
    }

    @Override
    public Subject afficher(int id) {
Subject r = new Subject();
     try {
         String query = "SELECT * FROM `subject` where id = ?";
         PreparedStatement ps = conn.prepareStatement(query);
         
         
         ps.setInt(1, id);
         
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                r.setId(rs.getInt("id"));
                r.setName(rs.getString("Name"));
                
                
//e.setUser(userser.show(rs.getInt("iduser")));

            }

         System.out.println("aaaa");
         System.out.println(r);
         
     } catch (SQLException ex) {
         Logger.getLogger(SubjectService.class.getName()).log(Level.SEVERE, null, ex);
         System.out.println("sqdsqd");
     }
        
return r ;
    }

    @Override
    public void modifier(Subject r ,int id) {
     try {
         String query = "UPDATE subject set nbrreserv=?  where id=?";
         PreparedStatement ps = conn.prepareStatement(query);
         
         ps.setInt(1, id);
         ps.setString(2, r.getName());
         
         ps.executeUpdate();
         System.out.println("Modification effectué");
     } catch (SQLException ex) {
         Logger.getLogger(SubjectService.class.getName()).log(Level.SEVERE, null, ex);
     }
    }

    
    //Affichage liste Reservation:
    public void AffichageSubject(Connection c, ObservableList<Subject> listeSubject) {
try {
           
          c= DATASOURCE.getInstance().getCnx();
           Statement statement = c.createStatement();
           ResultSet rs = statement.executeQuery(
                   
                  "SELECT `id`, `name` FROM `subject`"
           );
           while (rs.next()){
               listeSubject.add(new Subject(
                       rs.getInt("id"),
                        
                       rs.getString("Name"))
               );
           }
       } catch (SQLException ex) {
           Logger.getLogger(Subject.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

   
    @Override
    public ArrayList<Subject> afficherparId(int id) {
          
  
         ArrayList<Subject> eve = new ArrayList<Subject>();

         Subject r= new Subject();
        
        String resq = "select * from  subject where id = ? ";
        try {
            PreparedStatement ps = conn.prepareStatement(resq);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                eve.add(afficher(rs.getInt("id")));
                System.out.println(rs.getInt("id"));
                System.out.println(afficher(rs.getInt("id")));

                //e.setUser(userser.show(rs.getInt("iduser")));

            }

        } catch (SQLException ex) {
            Logger.getLogger(SubjectService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return eve;

}

  
    
}
    

