/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IServices.esprit;

import com.esprit.entity.Soutenance;
import com.esprit.entity.Subject;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Kaisser
 */
public interface ISubjectService  {
    public void add(Subject r);
    public void delete(int id);
    public Subject afficher(int id  );
     public ArrayList<Subject> afficherparId(int id);
    public void modifier(Subject r, int  id);
    
    public void Affichagesubject(Connection c , ObservableList<Subject>listeSubject);
    
   
    
    
    
}
